FROM ubuntu:18.04
USER root

RUN apt-get -y update \
    && apt-get -y install --no-install-recommends \
    curl \
    sudo \
	supervisor \
    vim \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get -y update \
    && apt-get install -y --no-install-recommends \
    mysql-server \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /run/mysqld && chown -R mysql:root /run/mysqld

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

EXPOSE 3306

CMD ["/usr/bin/supervisord"]
